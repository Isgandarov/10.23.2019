import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Task
{
    public static void main(String[] args) {
        ArrayList<Integer> even = new ArrayList<>();
        ArrayList<Integer> odd = new ArrayList<>();
        ArrayList<Integer> all = new ArrayList<>();
        Random rand = new Random();
        for(int i=0;i<100;i++){
            int num=rand.nextInt(100);
            if(num%2==0)
                even.add(num);
            if(num%2!=0)
                odd.add(num);
            all.add(num);
        }

        System.out.println("All: "+Arrays.toString(all.toArray()));
        System.out.println("EVEN: "+Arrays.toString(even.toArray()));
        System.out.println("ODD :"+Arrays.toString(odd.toArray()));
    }
}
